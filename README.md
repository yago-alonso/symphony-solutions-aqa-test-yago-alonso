# Symphony Solutions AQA test Yago Alonso

This repository contains automated tests for Yago Alonso Symphony Solutions AQA Test using Cypress.

## Requirements

- Node.js (version 18 or higher)
- NPM (Node Package Manager)
- Git 

## Getting Started

Follow the steps below to set up and run the tests:

If you have Git installed, you can clone the repository:

```bash
git clone https://gitlab.com/yago-alonso/symphony-solutions-aqa-test-yago-alonso.git

cd your-repository
npm install
npx cypress open

You can also run tests via the command line:
 npx cypress run --headless --browser electron --spec "cypress/e2e/**/*.cy.js"

## Running on CI/CD (GitLab CI/CD)

Tests are automatically executed on GitLab CI/CD after each push to the repository.

## Manual Execution of Pipeline in GitLab

In some cases, it may be necessary to manually execute the pipeline, for example, to perform specific tests or controlled deployments. Follow the steps below to run the pipeline in GitLab:

1. Open GitLab and navigate to the project.

2. In the left-hand sidebar, click on "CI/CD" and then "Pipelines".

3. You will see a list of previous and current pipelines. Select the pipeline you want to run manually.

4. In the top right corner of the pipeline page, click the "Run Pipeline" button.

5. Wait for the pipeline to complete. You can track the progress and view the results on the pipeline page.

