describe('API Test', () => {
  it('should fetch data from API and print API property of objects', () => {
      // Call the API and check the response
      cy.request('https://api.publicapis.org/entries')
          .its('body')
          .then((response) => {
              const authenticationObjects = response.entries.filter(
                  (entry) => entry.Category === 'Authentication & Authorization'
              );

              // Check if at least one object was found
              expect(authenticationObjects.length).to.be.greaterThan(0);

              // Check the count of objects
              const expectedCount = authenticationObjects.length;
              expect(authenticationObjects).to.have.lengthOf(expectedCount);

              // Print the API property of each found object
              authenticationObjects.forEach((obj, index) => {
                  cy.log(`Object ${index + 1} - API: ${obj.API}`);
              });
          });
  });
});
