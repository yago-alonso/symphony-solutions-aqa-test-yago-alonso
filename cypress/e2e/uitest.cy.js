describe('UI Test', () => {
  it('should verify sorting functionality', () => {
      cy.visit('https://www.saucedemo.com');
      // Step 2: Log in
      cy.get('#user-name').type('standard_user');
      cy.get('#password').type('secret_sauce');
      cy.get('#login-button').click();

      // Step 3: Verify items are sorted by Name (A -> Z)
      cy.get('.product_sort_container').select('az'); // Select A -> Z
      cy.get('.inventory_item_name').then(($items) => {
          const itemNames = $items.map((index, element) => Cypress.$(element).text()).get();
          expect(itemNames).to.deep.equal(itemNames.slice().sort());
      });

      // Step 4: Change sorting to Name (Z -> A)
      cy.get('.product_sort_container').select('za'); // Select Z -> A

      // Step 5: Verify items are sorted correctly
      cy.get('.inventory_item_name').then(($items) => {
          const itemNames = $items.map((index, element) => Cypress.$(element).text()).get();
          expect(itemNames).to.deep.equal(itemNames.slice().sort().reverse());
      });
  });
});
